//
//  ViewControllerViewModel.swift
//  Goxip
//
//  Created by Alex Ng on 22/2/2020.
//

import UIKit

protocol LooksViewModelLogic {
    func getInfluencers()
    func getFeeds(page: Int)
    func switchModeClicked()
}

class FeedInfo {
    var feeds: [Feed] = []
    var isFeedsProductShow: [Bool] = []
    var isLoadingFeeds: Bool = false
    var hvMoreFeeds: Bool = true
    var lastFeedsPage: Int = 0
}

class LooksViewModel: LooksViewModelLogic {

    private weak var vc: LooksViewControllerDisplayLogic?

    public var influencers: [Member] = []

    public var feedInfo: FeedInfo {
        return isFollowTab ? _followFeedInfo : _feedInfo
    }

    private var _feedInfo: FeedInfo = FeedInfo()
    private var _followFeedInfo: FeedInfo = FeedInfo()

    public var isGridMode: Bool = true
    public var isFollowTab: Bool = false {
        didSet {
            vc?.displayTabSwitch()
        }
    }

    required init(vc: LooksViewControllerDisplayLogic) {
        self.vc = vc
    }

    func getInfluencers() {
        GetInfluencersAPI().execute().done { influencers in
            self.influencers = influencers
            self.vc?.displayGetInfluencersSuccess()
        }.catch { e in
            self.vc?.displayError()
        }
    }

    func getFeeds(page: Int) {
        if _feedInfo.isLoadingFeeds { return }
        GetExploreFeedAPI(page: page).execute().done { feeds in
            self._feedInfo.isLoadingFeeds = true
            if page == 1 {
                self._feedInfo.lastFeedsPage = 0
                self._feedInfo.feeds = []
                self._feedInfo.isFeedsProductShow = []
            }

            self._feedInfo.hvMoreFeeds = feeds.count > 0
            self._feedInfo.lastFeedsPage = page
            self._feedInfo.feeds += feeds
            self._feedInfo.isFeedsProductShow += Array<Bool>(repeating: false, count: feeds.count)
            self.vc?.displayGetFeedsSuccess()
        }.ensure {
            self.vc?.displayEndRefreshing()
            self._feedInfo.isLoadingFeeds = false
        }.catch { e in
            self.vc?.displayError()
        }
    }

    func getFollowFeeds(page: Int) {
        if _followFeedInfo.isLoadingFeeds { return }
        self._followFeedInfo.isLoadingFeeds = true
        GetFollowFeedAPI(page: page).execute().done { feeds in
            if page == 1 {
                self._followFeedInfo.lastFeedsPage = 0
                self._followFeedInfo.feeds = []
                self._followFeedInfo.isFeedsProductShow = []
            }

            self._followFeedInfo.hvMoreFeeds = feeds.count > 0
            self._followFeedInfo.lastFeedsPage = page
            self._followFeedInfo.feeds += feeds
            self._followFeedInfo.isFeedsProductShow += Array<Bool>(repeating: false, count: feeds.count)
            self.vc?.displayGetFeedsSuccess()
        }.ensure {
            self.vc?.displayEndRefreshing()
            self._followFeedInfo.isLoadingFeeds = false
        }.catch { e in
            self.vc?.displayError()
        }
    }

    func switchModeClicked() {
        isGridMode.toggle()
        vc?.displayFeedMode(isGridMode)
    }
}
