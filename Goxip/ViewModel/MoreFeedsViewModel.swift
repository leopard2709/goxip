//
//  MoreFeedsViewModel.swift
//  Goxip
//
//  Created by Alex Ng on 26/2/2020.
//

import UIKit

protocol MoreFeedsViewModelLogic {
    func getRelatedFeeds(id: Int, page: Int)
}

class MoreFeedsViewModel: MoreFeedsViewModelLogic {
    
    private weak var vc: MoreFeedsViewControllerDisplayLogic?

    public var feedInfo = FeedInfo()

    required init(vc: MoreFeedsViewControllerDisplayLogic) {
        self.vc = vc
    }

    func getRelatedFeeds(id: Int, page: Int) {
        if feedInfo.isLoadingFeeds { return }
        self.feedInfo.isLoadingFeeds = true
        GetRelatedFeedsAPI(id: id, page: page).execute().done { feeds in
            if page == 1 {
                self.feedInfo.lastFeedsPage = 0
                self.feedInfo.feeds = []
                self.feedInfo.isFeedsProductShow = []
            }

            self.feedInfo.hvMoreFeeds = feeds.count > 0
            self.feedInfo.lastFeedsPage = page
            self.feedInfo.feeds += feeds
            self.feedInfo.isFeedsProductShow += Array<Bool>(repeating: false, count: feeds.count)
            self.feedInfo.isLoadingFeeds = false
            self.vc?.displayGetFeedsSuccess()
        }.ensure {
            self.feedInfo.isLoadingFeeds = false
        }.catch { e in
            self.vc?.displayError()
        }
    }
}
