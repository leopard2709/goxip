//
//  LooksViewController.swift
//  Goxip
//
//  Created by Alex Ng on 21/2/2020.
//

import UIKit
import Kingfisher

protocol LooksViewControllerDisplayLogic: class {
    func displayGetInfluencersSuccess()
    func displayGetFeedsSuccess()
    func displayEndRefreshing()
    func displayError()
    func displayFeedMode(_ isGrid: Bool)
    func displayTabSwitch()
}

class LooksViewController: UIViewController {

    @IBOutlet var headerView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var switchModeButton: UIButton!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tabUnderlineLeftConstraint: NSLayoutConstraint!

    fileprivate let influencerCellIdentifier = "influencerCell"
    fileprivate let gridFeedTableViewCellIdentifier = "gridFeedCeell"
    fileprivate let feedTableViewCellIdentifier = "feedCeell"

    private var viewModel: LooksViewModel!
    private var refreshControl = UIRefreshControl()

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionViewHeightConstraint.constant = (view.bounds.width / 3) * 2
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        setupCollectionView()
        setupTableView()
        setupRefreshControl()

        viewModel.getInfluencers()
        viewModel.getFeeds(page: 1)
        viewModel.getFollowFeeds(page: 1)
    }

    private func setup() {
        viewModel = LooksViewModel(vc: self)
    }

    private func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self

        tableView.separatorStyle = .none
        tableView.estimatedRowHeight = UITableView.automaticDimension

        tableView.estimatedSectionHeaderHeight = UITableView.automaticDimension

        tableView.register(FeedGridTableViewCell.fromNib(), forCellReuseIdentifier: gridFeedTableViewCellIdentifier)
        tableView.register(FeedTableViewCell.fromNib(), forCellReuseIdentifier: feedTableViewCellIdentifier)
    }

    private func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self

        collectionView.isPagingEnabled = true
        collectionView.showsHorizontalScrollIndicator = false

        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        collectionView.collectionViewLayout = layout

        collectionView.register(InfluencerCollectionViewCell.fromNib(), forCellWithReuseIdentifier: influencerCellIdentifier)
    }

    private func setupRefreshControl() {
        tableView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(reloadData), for: .valueChanged)
    }

    @objc private func reloadData() {
        viewModel.getFeeds(page: 1)
        viewModel.getFollowFeeds(page: 1)
        viewModel.getInfluencers()
    }

    @IBAction func switchGridClicked(_ sender: Any) {
        viewModel.switchModeClicked()
    }

    @IBAction func feedClicked(_ sender: UIButton) {
        tabUnderlineLeftConstraint.constant = sender.frame.minX
        viewModel.isFollowTab = false
    }

    @IBAction func followClicked(_ sender: UIButton) {
        tabUnderlineLeftConstraint.constant = sender.frame.minX
        viewModel.isFollowTab = true
    }
}

extension LooksViewController: LooksViewControllerDisplayLogic {

    func displayTabSwitch() {
        tableView.reloadData()
    }

    func displayFeedMode(_ isGrid: Bool) {
        switchModeButton.setTitle(isGrid ? "Normal" : "Grid", for: .normal)
        let count = tableView.indexPathsForVisibleRows?.count ?? 0
        let index = (count - 1) / 2
        if tableView.indexPathsForVisibleRows?.indices.contains(index) == false { return }

        let middleRow = tableView.indexPathsForVisibleRows?[index].row ?? 0
        let row = isGrid ? middleRow / 2 : middleRow * 2
        tableView.reloadData()
        if row + 1 > tableView.numberOfRows(inSection: 0) {
            return
        }
        tableView.scrollToRow(at: IndexPath(row: row, section: 0), at: .middle, animated: false)
    }

    func displayGetFeedsSuccess() {
        tableView.reloadData()
    }

    func displayGetInfluencersSuccess() {
        collectionView.reloadData()
    }

    func displayEndRefreshing() {
        refreshControl.endRefreshing()
    }

    func displayError() {
        let alert = UIAlertController(title: "Something went wrong!", message: "Please try again", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}

extension LooksViewController: FeedTableViewCellLogic {

    func imageClicked(feedId: Int?) {
        let feed = viewModel.feedInfo.feeds.filter({ $0.id == feedId})
        let vc = MoreFeedViewController.initFromXIB()
        vc.feed = feed.first
        navigationController?.pushViewController(vc, animated: true)
    }

    func showProductClicked(leftId: Int?, rightId: Int?, clickedId: Int?) {
        if let id = leftId,
            let leftIndex = viewModel.feedInfo.feeds.firstIndex(where: { $0.id == id }) {
            if clickedId == leftId {
                viewModel.feedInfo.isFeedsProductShow[leftIndex].toggle()
            } else {
                viewModel.feedInfo.isFeedsProductShow[leftIndex] = false
            }
        }

        if let id = rightId,
            let rightIndex = viewModel.feedInfo.feeds.firstIndex(where: { $0.id == id }) {
            if clickedId == rightId {
                viewModel.feedInfo.isFeedsProductShow[rightIndex].toggle()
            } else {
                viewModel.feedInfo.isFeedsProductShow[rightIndex] = false
            }
        }
        tableView.reloadData()
    }
}

extension LooksViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.influencers.count
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = tableView.bounds.width / 3
        return CGSize(width: size - 10, height: size - 10)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let _cell = collectionView.dequeueReusableCell(withReuseIdentifier: influencerCellIdentifier, for: indexPath)
        guard let cell = _cell as? InfluencerCollectionViewCell else { return _cell }

        let influencer = viewModel.influencers[indexPath.row]
        cell.titleLabel.text = influencer.name
        cell.imageView.setImageWithUrl(influencer.imageUrl)
        return cell
    }
}

extension LooksViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count: Double = Double(viewModel.feedInfo.feeds.count) / 2
        let gridCount = Int(count.rounded(.down))
        return viewModel.isGridMode ? gridCount : viewModel.feedInfo.feeds.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = viewModel.isGridMode ? gridFeedTableViewCellIdentifier : feedTableViewCellIdentifier
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier)

        if let cell = cell as? FeedGridTableViewCell {
            return configGridFeedCell(cell, indexPath: indexPath)
        } else if let cell = cell as? FeedTableViewCell {
            return configFeedCell(cell, indexPath: indexPath)
        } else {
            return cell ?? UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if viewModel.isGridMode { return }
        navigationController?.pushViewController(MoreFeedViewController.initFromXIB(), animated: true)
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if viewModel.feedInfo.hvMoreFeeds && !viewModel.feedInfo.isLoadingFeeds,
            (indexPath.row + 1) * 2 == viewModel.feedInfo.feeds.count {
            if viewModel.isFollowTab {
                viewModel.getFollowFeeds(page: viewModel.feedInfo.lastFeedsPage + 1)
            } else {
                viewModel.getFeeds(page: viewModel.feedInfo.lastFeedsPage + 1)
            }
        }
    }

    private func configFeedCell(_ cell: FeedTableViewCell, indexPath: IndexPath) -> FeedTableViewCell {
        let feed = viewModel.feedInfo.feeds[indexPath.row]
        cell.setFeed(feed)
        return cell
    }

    private func configGridFeedCell(_ cell: FeedGridTableViewCell, indexPath: IndexPath) -> FeedGridTableViewCell  {
        let feeds = viewModel.feedInfo.feeds
        let row = indexPath.row * 2
        let leftIndex = row
        let rightIndex = row + 1
        let leftFeed = feeds.indices.contains(leftIndex) ? feeds[leftIndex] : nil
        let rightFeed = feeds.indices.contains(rightIndex) ? feeds[rightIndex] : nil

        var products: [Product] = []
        if viewModel.feedInfo.isFeedsProductShow[leftIndex] {
            products = leftFeed?.products ?? []
        }

        if viewModel.feedInfo.isFeedsProductShow[rightIndex] {
            products = rightFeed?.products ?? []
        }

        cell.leftFeedId = leftFeed?.id
        cell.rightFeedId = rightFeed?.id
        cell.collectionView.products = products

        cell.leftImageView.setImageWithUrl(leftFeed?.image)
        cell.rightImageView.kf.cancelDownloadTask()
        cell.rightImageView.kf.setImage(with: URL(string: rightFeed?.image ?? ""))

        cell.delegate = self
        return cell
    }
}
