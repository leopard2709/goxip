//
//  MoreFeedViewController.swift
//  Goxip
//
//  Created by Alex Ng on 26/2/2020.
//

import UIKit

protocol MoreFeedsViewControllerDisplayLogic: class {
    func displayError()
    func displayGetFeedsSuccess()
}

class MoreFeedViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var collectionView: UICollectionView!

    private var viewModel: MoreFeedsViewModel!

    fileprivate let moreFeedCellIdentifier = "moreFeedCell"
    fileprivate let moreFeedHeaderIdentifier = "headerCell"
    fileprivate let cellIdentifier = "feedCell"

    public var feed: Feed?

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        setupNavBarImage()
        setupCollectionView()
        setupTableView()

        viewModel.getRelatedFeeds(id: feed?.member?.id ?? 0, page: 1)
        title = feed?.member?.name
    }

    private func setup() {
        viewModel = MoreFeedsViewModel(vc: self)
    }

    private func setupNavBarImage() {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        let buttonItem = UIBarButtonItem(customView: imageView)

        navigationItem.rightBarButtonItem = buttonItem
        imageView.setImageWithUrl(feed?.member?.imageUrl)
        imageView.makeCircle()
    }

    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self

        tableView.separatorStyle = .none
        tableView.estimatedRowHeight = UITableView.automaticDimension

        tableView.register(FeedTableViewCell.fromNib(), forCellReuseIdentifier: cellIdentifier)
    }

    private func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self

        collectionView.register(MoreFeedCollectionViewCell.fromNib(), forCellWithReuseIdentifier: moreFeedCellIdentifier)
    }
}

extension MoreFeedViewController: MoreFeedsViewControllerDisplayLogic {

    func displayGetFeedsSuccess() {
        tableView.reloadData()
        collectionView.reloadData()
    }

    func displayError() {
        let alert = UIAlertController(title: "Something went wrong!", message: "Please try again", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}

extension MoreFeedViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let _cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        guard let cell = _cell as? FeedTableViewCell,
            let feed = feed else { return _cell }
        cell.setFeed(feed)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let rowsCount = (Double(viewModel.feedInfo.feeds.count) / 2).rounded(.down)
        // (collectionCellSize + edgeLeft + edgeRight)
        return ((tableView.frame.width / 2 - 20) + 5 + 5) * CGFloat(rowsCount)
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return collectionView
    }
}

extension MoreFeedViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.feedInfo.feeds.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: moreFeedCellIdentifier, for: indexPath)
        cell.backgroundColor = .red
        let feed = viewModel.feedInfo.feeds[indexPath.row]
        (cell as? MoreFeedCollectionViewCell)?.imageView.setImageWithUrl(feed.image)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (collectionView.frame.width / 2) - 20
        return CGSize(width: size, height: size)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if viewModel.feedInfo.hvMoreFeeds && !viewModel.feedInfo.isLoadingFeeds,
            indexPath.row + 1 == viewModel.feedInfo.feeds.count {
            viewModel.getRelatedFeeds(id: feed?.member?.id ?? 0, page: viewModel.feedInfo.lastFeedsPage + 1)
        }
    }
}
