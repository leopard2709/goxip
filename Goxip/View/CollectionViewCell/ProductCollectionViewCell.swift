//
//  ProductCollectionViewCell.swift
//  Goxip
//
//  Created by Alex Ng on 23/2/2020.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
