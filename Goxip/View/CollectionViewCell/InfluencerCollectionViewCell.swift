//
//  InfluencerCollectionViewCell.swift
//  Goxip
//
//  Created by Alex Ng on 22/2/2020.
//

import UIKit

class InfluencerCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!

    override func layoutSubviews() {
        super.layoutSubviews()

        imageView.makeCircle()
    }

    override func awakeFromNib() {
        super.awakeFromNib()

    }
}
