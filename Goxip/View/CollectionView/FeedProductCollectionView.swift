//
//  FeedProductCollectionView.swift
//  Goxip
//
//  Created by Alex Ng on 25/2/2020.
//

import UIKit

class FeedProductCollectionView: UICollectionView {

    fileprivate let cellIdentifier = "productCell"

    public var products: [Product] = [] {
        didSet {
            setContentOffset(.zero, animated: false)
            reloadData()
            isHidden = self.products.count == 0
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupCollectionView()
    }

    private func setupCollectionView() {
        delegate = self
        dataSource = self

        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        collectionViewLayout = layout

        showsHorizontalScrollIndicator = false
        register(ProductCollectionViewCell.fromNib(), forCellWithReuseIdentifier: cellIdentifier)
    }
}

extension FeedProductCollectionView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let _cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath)
        guard let cell = _cell as? ProductCollectionViewCell else { return _cell }
        let product = products[indexPath.row]
        cell.imageView.setImageWithUrl(product.image)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.height - 10, height: collectionView.bounds.height - 10)
    }
}
