//
//  FeedTableViewCell.swift
//  Goxip
//
//  Created by Alex Ng on 25/2/2020.
//

import UIKit

class FeedTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var feedImageView: UIImageView!
    @IBOutlet weak var collectionView: FeedProductCollectionView!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!

    private var comments: [FeedComment] = [] {
        didSet { tableView.reloadData() }
    }

    private let cellIdentifier = "cell"

    override func layoutSubviews() {
        super.layoutSubviews()

        profileImageView.makeCircle()
        tableHeightConstraint.constant = tableView.contentSize.height
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        setupTableView()

        selectionStyle = .none
    }

    public func setFeed(_ feed: Feed) {
        profileImageView.setImageWithUrl(feed.member?.imageUrl)
        feedImageView.setImageWithUrl(feed.image)
        collectionView.products = feed.products ?? []
        nameLabel.text = feed.member?.name ?? ""
        commentLabel.text = feed.description ?? ""
        comments = feed.comments ?? []
    }

    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self

        tableView.bounces = false
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        tableView.frame.size.height = tableView.contentSize.height
        tableView.separatorStyle = .none
    }
}

extension FeedTableViewCell: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        cell?.selectionStyle = .none
        let comment = comments[indexPath.row]
        cell?.textLabel?.text = (comment.member?.name ?? "") + ": " + (comment.text ?? "")
        return cell ?? UITableViewCell()
    }
}
