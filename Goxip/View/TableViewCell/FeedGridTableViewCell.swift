//
//  FeedGridTableViewCell.swift
//  Goxip
//
//  Created by Alex Ng on 22/2/2020.
//

import UIKit

protocol FeedTableViewCellLogic: class {
    func showProductClicked(leftId: Int?, rightId: Int?, clickedId: Int?)
    func imageClicked(feedId: Int?)
}

class FeedGridTableViewCell: UITableViewCell {

    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var rightView: UIView!

    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var rightImageView: UIImageView!

    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!

    @IBOutlet weak var bottomStackView: UIStackView!
    @IBOutlet weak var collectionView: FeedProductCollectionView!

    public weak var delegate: FeedTableViewCellLogic?
    public var leftFeedId: Int?
    public var rightFeedId: Int?

    fileprivate let cellIdentifier = "productCell"

    override func awakeFromNib() {
        super.awakeFromNib()

        setupUI()
    }

    private func setupUI() {
        selectionStyle = .none
        collectionView.isHidden = true

        leftImageView.isUserInteractionEnabled = true
        rightImageView.isUserInteractionEnabled = true

        leftImageView?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(leftImageClicked)))
        rightImageView?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(rightImageClicked)))
    }
    
    @IBAction func showProductClicked(_ sender: UIButton) {
        var feedId: Int?
        switch sender {
        case leftButton:
            feedId = leftFeedId
        case rightButton:
            feedId = rightFeedId
        default:
            break
        }
        delegate?.showProductClicked(leftId: leftFeedId, rightId: rightFeedId, clickedId: feedId)
    }

    @objc func leftImageClicked() {
        delegate?.imageClicked(feedId: leftFeedId)
    }

    @objc func rightImageClicked() {
        delegate?.imageClicked(feedId: rightFeedId)
    }
}
