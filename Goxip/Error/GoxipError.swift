//
//  GoxipError.swift
//  Goxip
//
//  Created by Alex Ng on 21/2/2020.
//

enum GoxipError: Error {
    case objectMapper
}
