//
//  Influencer.swift
//  Goxip
//
//  Created by Alex Ng on 23/2/2020.
//

import ObjectMapper

struct Member: Mappable {

    var id: Int?
    var name: String?
    var userName: String?
    var imageUrl: String?
    var type: Type?
    var isFollowing: Bool?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        id          <- map["id"]
        name        <- map["name"]
        userName    <- map["username"]
        imageUrl    <- map["profile_image"]
        type        <- (map["member_type"], EnumTransform<Type>())
        isFollowing <- map["is_following"]
    }
}

extension Member {
    enum `Type`: String {
        case influencer = "influencer"
        case standard   = "standard"
    }
}
