//
//  Product.swift
//  Goxip
//
//  Created by Alex Ng on 23/2/2020.
//

import ObjectMapper

struct Product: Mappable {

    var id: Int?
    var image: String?
    var shopLink: String?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        id          <- map["id"]
        image       <- map["main_image"]
        shopLink    <- map["override_shop_link"]
    }
}
