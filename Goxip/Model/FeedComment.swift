//
//  FeedComment.swift
//  Goxip
//
//  Created by Alex Ng on 23/2/2020.
//

import ObjectMapper

struct FeedComment: Mappable {

    var id: Int?
    var postId: Int?
    var member: Member?
    var text: String?
    var createdAt: String?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        id          <- map["id"]
        postId      <- map["post_id"]
        member      <- map["member"]
        text        <- map["text"]
        createdAt   <- map["created_at"]
    }
}
