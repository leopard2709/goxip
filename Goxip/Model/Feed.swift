//
//  Feed.swift
//  Goxip
//
//  Created by Alex Ng on 23/2/2020.
//

import ObjectMapper

struct Feed: Mappable {
    var id: Int?
    var member: Member?
    var image: String?
    var description: String?
    var link: String?
    var isLiked: Bool?
    var totalLike: Int?
    var totalComment: Int?
    var products: [Product]?
    var comments: [FeedComment]?
    var createdAt: String?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        id              <- map["id"]
        member          <- map["member"]
        image           <- map["main_image"]
        description     <- map["description"]
        link            <- map["link"]
        isLiked         <- map["liked"]
        totalLike       <- map["total_like"]
        totalComment    <- map["total_comment"]
        products        <- map["products"]
        comments        <- map["comments"]
        createdAt       <- map["created_at"]
    }
}

