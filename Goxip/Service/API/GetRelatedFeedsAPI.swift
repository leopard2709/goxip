//
//  GetRelatedFeedsAPI.swift
//  Goxip
//
//  Created by Alex Ng on 26/2/2020.
//

import Alamofire
import ObjectMapper
import PromiseKit

class GetRelatedFeedsAPI: APIRequestProtocl {
    var method: HTTPMethod = .get
    var url: String = "/api/v1/members/{member_id}/posts?page={page}"

    required init(id: Int, page: Int) {
        url = url.replacingOccurrences(of: "{page}", with: String(page))
            .replacingOccurrences(of: "{member_id}", with: String(id))
    }

    public func execute() -> Promise<[Feed]> {
        return Promise { r in
            BaseService.request(self).done { (response) in
                guard let obj = Mapper<Feed>().mapArray(JSONObject: response) else {
                    r.reject(GoxipError.objectMapper)
                    return
                }
                r.fulfill(obj)
            }.catch { e in
                r.reject(e)
            }
        }
    }
}
