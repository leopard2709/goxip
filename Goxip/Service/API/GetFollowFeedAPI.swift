//
//  GetFollowFeedAPI.swift
//  Goxip
//
//  Created by Alex Ng on 26/2/2020.
//

import Alamofire
import ObjectMapper
import PromiseKit

class GetFollowFeedAPI: APIRequestProtocl {
    var method: HTTPMethod = .get
    var url: String = "/api/v1/posts/following?page={page}"

    required init(page: Int) {
        url = url.replacingOccurrences(of: "{page}", with: String(page))
    }

    public func execute() -> Promise<[Feed]> {
        return Promise { r in
            BaseService.request(self).done { (response) in
                guard let obj = Mapper<Feed>().mapArray(JSONObject: response) else {
                    r.reject(GoxipError.objectMapper)
                    return
                }
                r.fulfill(obj)
            }.catch { e in
                r.reject(e)
            }
        }
    }
}
