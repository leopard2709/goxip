//
//  GetInfluencersAPI.swift
//  Goxip
//
//  Created by Alex Ng on 21/2/2020.
//

import Alamofire
import ObjectMapper
import PromiseKit

class GetInfluencersAPI: APIRequestProtocl {
    var method: HTTPMethod = .get
    var url: String = "/api/v1/members/featured"

    public func execute() -> Promise<[Member]> {
        return Promise { r in
            BaseService.request(self).done { (response) in
                guard let obj = Mapper<Member>().mapArray(JSONObject: response) else {
                    r.reject(GoxipError.objectMapper)
                    return
                }
                r.fulfill(obj)
            }.catch { e in
                r.reject(e)
            }
        }
    }
}
