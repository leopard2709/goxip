//
//  BaseService.swift
//  Goxip
//
//  Created by Alex Ng on 21/2/2020.
//

import Alamofire
import ObjectMapper
import PromiseKit

protocol APIRequestProtocl {
    var method: HTTPMethod { get set }
    var url: String { get set }
}

class BaseService: NSObject {

    static let endPoint = "http://134.209.98.222"

    static public func request(_ request: APIRequestProtocl) -> Promise<Any> {
        return Promise { r in
            AF.request(endPoint + request.url, method: request.method, parameters: nil, encoding: JSONEncoding.default).responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    r.fulfill(value)
                case .failure(let error):
                    r.reject(error)
                }
            }
        }
    }
}
