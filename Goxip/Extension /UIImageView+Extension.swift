//
//  UIImageView+Extension.swift
//  Goxip
//
//  Created by Alex Ng on 25/2/2020.
//

import Kingfisher

extension UIImageView {

    func setImageWithUrl(_ url: String?) {
        kf.cancelDownloadTask()
        kf.setImage(with: URL(string: url ?? ""))
    }
}
