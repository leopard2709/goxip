//
//  UIView+Extension.swift
//  Goxip
//
//  Created by Alex Ng on 22/2/2020.
//

import UIKit

extension UIView {
    
    public func makeCircle() {
        layoutIfNeeded()
        layer.cornerRadius = bounds.height / 2
        clipsToBounds = true
    }
}
