//
//  Nib+Extension.swift
//  Goxip
//
//  Created by Alex Ng on 22/2/2020.
//

import UIKit

extension UICollectionReusableView {
    public class func fromNib() -> UINib {
        let bundle = Bundle(for: self)
        return UINib(nibName: String(describing: self), bundle: bundle)
    }
}

extension UITableViewCell {
    public class func fromNib() -> UINib {
        let bundle = Bundle(for: self)
        return UINib(nibName: String(describing: self), bundle: bundle)
    }
}

extension UIViewController {
    public class func initFromXIB() -> Self {
        func helper<T>() -> T where T: UIViewController {
            return T(nibName: String(describing: self), bundle: nil)
        }
        return helper()
    }
}
