- Install Cocoapods library

`$ cd {Project Dictionary}`  
`$ pod install`  
`open Goxip.xcworkspace`

- Why use this library

`ObjectMapper - handle the JSON to object`  
`Alamofire 	 - handle HTTP request`  
`PromiseKit	 - handle async programming`  
`Kingfisher   - handle download image and cache`  